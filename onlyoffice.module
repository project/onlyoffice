<?php

/**
 * @file
 * Primary module hooks for ONLYOFFICE Connector module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\onlyoffice\OnlyofficeDocumentHelper;
use Drupal\onlyoffice\OnlyofficeAppConfig;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\onlyoffice\OnlyofficeUrlHelper;

/**
 * Implements hook_help().
 */
function onlyoffice_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.onlyoffice':
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The ONLYOFFICE connector allows users to edit office documents, spreadsheets, and presentations in the Media module from Drupal using ONLYOFFICE Docs packaged as Document Server. Users are able to collaborate on documents in real-time and preview files on public pages.') . '</p>';

      $output .= '<h3>' . t('Installation and configuration') . '</h3>';
      $output .= '<p>' . t('Explore how to install and configure the ONLYOFFICE connector for Drupal on the <a href=":onlyoffice-drupal">official project page</a>.', [':onlyoffice-drupal' => 'https://github.com/ONLYOFFICE/onlyoffice-drupal']) . '</p>';

      return $output;
  }
}

/**
 * Implements hook_theme().
 */
function onlyoffice_theme($existing, $type, $theme, $path) {
  return [
    'onlyoffice_editor' => [
      'variables' => [
        'filename' => 'noname',
        'favicon_path' => 'none',
        'doc_server_url' => 'http://127.0.0.1/',
        'config' => '{}',
        'error' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_entity_operation().
 */
function onlyoffice_entity_operation(EntityInterface $entity) {
  if ($entity->getEntityTypeId() != "media") {
    return [];
  }
  /** @var \Drupal\media\Entity\Media $media */
  $media = $entity;

  if ($media->getSource()->getPluginId() != "file") {
    return [];
  }

  $account = \Drupal::currentUser()->getAccount();

  if (!$media->access("view", $account)) {
    return [];
  }

  $file = $media->get(OnlyofficeDocumentHelper::getSourceFieldName($media))->entity;
  $extension = OnlyofficeDocumentHelper::getExtension($file->getFilename());

  if (OnlyofficeDocumentHelper::getDocumentType($extension) == NULL) {
    return [];
  }

  $title = t("View in ONLYOFFICE");

  if (OnlyofficeDocumentHelper::isEditable($media) && $media->access("update", $account)) {
    $title = t("Edit in ONLYOFFICE");
  }

  return [
    'onlyoffice' => [
      'title' => $title,
      'weight' => 50,
      'url' => OnlyofficeUrlHelper::getEditorUrl($media),
    ],
  ];
}

/**
 * Implements hook_library_info_build().
 */
function onlyoffice_library_info_build() {

  $options = \Drupal::config('onlyoffice.settings');

  if ($doc_server_url = $options->get('doc_server_url')) {
    $api_url = $doc_server_url . OnlyofficeAppConfig::getDocServiceApiUrl();
    return [
      'onlyoffice.api' => [
        'js' => [
          $api_url => [],
        ],
      ],
    ];
  }
}
